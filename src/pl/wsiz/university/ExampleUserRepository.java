package pl.wsiz.university;

import pl.wsiz.university.domain.Student;
import pl.wsiz.university.domain.Teacher;
import pl.wsiz.university.domain.User;
import pl.wsiz.university.domain.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ExampleUserRepository implements UserRepository {

    private List<User> memory;

    public ExampleUserRepository() {
        memory = new ArrayList<>();
        memory.add(new Student("Andrzej", "Bobowski",
                "janek342@gmail.com", "Jan@13", 344421L));
        memory.add(new Teacher("Aldona", "Kowalska",
                "kowalska@gmail.com", "malg@onE", "mgr"));
    }

    @Override
    public void insert(User user) {
        memory.add(user);
    }

    @Override
    public List<User> findAll() {
        return memory;
    }

    @Override
    public Optional<User> findByEmailAndPassword(String email, String password) {
        List<User> users = findAll();
        for (User user: users) {
            if (user.getEmail().equalsIgnoreCase(email)
                    && user.getPassword().equals(password)) {
                return Optional.of(user);
            }
        }
        return Optional.empty();
    }

}
