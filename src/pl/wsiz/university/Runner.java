package pl.wsiz.university;

import pl.wsiz.university.domain.*;
import pl.wsiz.university.security.LoginForm;
import pl.wsiz.university.view.UserListView;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

public class Runner {

    public static void main(String[] args) {
//        testFile();
        UserRepository userRepository = new FileUserRepository();
        LoginForm loginForm = new LoginForm(userRepository);
        loginForm.run();
    }

    private static void testFile() {
        Student student1 = new Student("Jan", "Bobowski",
                "janek342@gmail.com", "Jan@13", 344421L);
        Teacher teacher1 = new Teacher("Małgorzata", "Kowalska",
                "kowalska@gmail.com", "malg@onE", "mgr");
        Administrator administrator1 = new Administrator("Witold", "Kowalski",
                "witek35@gmail.com", "Qwert@e9x3");
        Administrator administrator2 = new Administrator("Witold", "Bobowski",
                "witek35@gmail.com", "Qwert@e9x3");

        UserRole role1 = UserRole.STUDENT;
        UserRole role2 = UserRole.TEACHER;
        UserRole role3 = UserRole.ADMINISTRATOR;

        FileUserRepository fileUserRepository = new FileUserRepository();
        fileUserRepository.insert(student1);
        fileUserRepository.insert(teacher1);
        fileUserRepository.insert(administrator1);
        fileUserRepository.insert(administrator2);
        System.out.println(fileUserRepository.findAll());

        int firstNameMaxLength = UserListView.FIRST_NAME_MAX_LENGTH;
        System.out.println(firstNameMaxLength);

        Pair<Integer, String> pair1 = new Pair<>(1, "text1");
        Integer left1 = pair1.getLeft();
        String right1 = pair1.getRight();
        System.out.println(left1);
        System.out.println(right1);
        int length = right1.length();
        System.out.println(length);

        Pair<String, UserRole> pair2 = new Pair<>("1", UserRole.ADMINISTRATOR);
        String left2 = pair2.getLeft();
        UserRole right2 = pair2.getRight();
        System.out.println(left2);
        System.out.println(right2);

        Function<String, Integer> function1 = s -> s.length();
        Integer length2 = function1.apply("tekst");

        Consumer<String> consumer1 = s -> System.out.println(s);
        consumer1.accept("tekst");

        String s1 = new String("33");

        String s2 = null;

        Optional<String> optionalS3 = Optional.empty();
        String s3 = optionalS3.orElse("1ihiuhui");
        System.out.println(s3.length());
    }

}
