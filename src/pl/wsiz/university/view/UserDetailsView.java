package pl.wsiz.university.view;

import pl.wsiz.university.domain.User;
import pl.wsiz.university.domain.UserRepository;

public abstract class UserDetailsView<T extends User> extends BaseView {

    private UserRepository userRepository;

    protected UserDetailsView(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    protected void initialize() {
        T user = createUser();
        userRepository.insert(user);
        System.out.println("Użytkownik "+user.getFirstName()+" "+user.getLastName()+" został wstawiony");
    }

    protected abstract T createUser();

}
