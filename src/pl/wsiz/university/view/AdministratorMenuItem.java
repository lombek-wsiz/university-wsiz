package pl.wsiz.university.view;

public enum AdministratorMenuItem {

    USER_LIST(1),
    ADMINISTRATOR_ADD(2),
    TEACHER_ADD(3),
    STUDENT_ADD(4),
    EXIT(5);

    private int nr;

    AdministratorMenuItem(int nr) {
        this.nr = nr;
    }

    public static AdministratorMenuItem ofNr(int nr) {
        AdministratorMenuItem[] items = values();
        for (AdministratorMenuItem item: items) {
            if (item.nr == nr) {
                return item;
            }
        }
        return AdministratorMenuItem.EXIT;
    }

    public String getTranslated() {
        switch (this) {
            case USER_LIST:
                return "lista użytkowników";
            case ADMINISTRATOR_ADD:
                return "dodaj administratora";
            case TEACHER_ADD:
                return "dodaj nauczyciela";
            case STUDENT_ADD:
                return "dodaj studenta";
            case EXIT:
                return "wyjście z programu";
        }
        throw new RuntimeException("Not supported");
    }

    public int getNr() {
        return nr;
    }

}
