package pl.wsiz.university.view;

import pl.wsiz.university.domain.User;
import pl.wsiz.university.domain.UserComparator;
import pl.wsiz.university.domain.UserRepository;

import java.util.List;

public class UserListView extends BaseView {

    public static final int FIRST_NAME_MAX_LENGTH = 20;
    public static final int LAST_NAME_MAX_LENGTH = 20;
    public static final int EMAIL_MAX_LENGTH = 24;
    public static final int ROLE_MAX_LENGTH = 10;

    private UserRepository userRepository;

    public UserListView(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public void initialize() {
        System.out.println(
                withSpaces("Imię", FIRST_NAME_MAX_LENGTH)
                        + withSpaces("Nazwisko", LAST_NAME_MAX_LENGTH)
                        + withSpaces("E-mail", EMAIL_MAX_LENGTH)
                        + withSpaces("Funkcja", ROLE_MAX_LENGTH)
        );
        List<User> users = userRepository.findAll();
        users.sort(new UserComparator());
        users.forEach(user -> System.out.println(
                withSpaces(user.getFirstName()  , FIRST_NAME_MAX_LENGTH)
                        + withSpaces(user.getLastName(), LAST_NAME_MAX_LENGTH)
                        + withSpaces(user.getEmail(), EMAIL_MAX_LENGTH)
                        + withSpaces(user.getRole().getTranslated(), ROLE_MAX_LENGTH)
        ));
    }

    private String withSpaces(String text, int maxLength) {
        String result = text; // or StringBuilder
        int length = text.length();
        int spaces = maxLength - length;
        for (int i = 1; i <= spaces; i++) {
            result += " ";
        }
        return result;
    }

    @Override
    public String getTitle() {
        return "LISTA UŻYTKOWNIKÓW";
    }

}
