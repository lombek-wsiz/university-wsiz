package pl.wsiz.university.view;

import pl.wsiz.university.domain.UserRepository;

import java.util.Scanner;

public class AdministratorMenuView extends SystemMenuView {

    private UserRepository userRepository;

    public AdministratorMenuView(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    protected void initialize() {
        AdministratorMenuItem[] items = AdministratorMenuItem.values();
        for (AdministratorMenuItem item : items) {
            System.out.println(item.getNr() + " - " + item.getTranslated());
        }
        System.out.println("Twój wybór: ");
        Scanner scanner = new Scanner(System.in);
        int nr = scanner.nextInt();
        AdministratorMenuItem menuItem = AdministratorMenuItem.ofNr(nr);
        switch (menuItem) {
            case USER_LIST:
                UserListView userListView = new UserListView(userRepository);
                userListView.run();
                run();
                break;
            case ADMINISTRATOR_ADD:
                AdministratorDetailsView administratorDetailsView = new AdministratorDetailsView(userRepository);
                administratorDetailsView.run();
                run();
                break;
            case TEACHER_ADD:
                TeacherDetailsView teacherDetailsView = new TeacherDetailsView(userRepository);
                teacherDetailsView.run();
                run();
                break;
            case STUDENT_ADD:
                StudentDetailsView studentDetailsView = new StudentDetailsView(userRepository);
                studentDetailsView.run();
                run();
                break;
        }
    }

    @Override
    protected String getTitle() {
        return "MENU ADMINISTRATORA";
    }

}
