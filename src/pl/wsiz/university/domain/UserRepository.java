package pl.wsiz.university.domain;

import java.util.List;
import java.util.Optional;

public interface UserRepository {

    void insert(User user);
    List<User> findAll();
    Optional<User> findByEmailAndPassword(String email, String password);

}
